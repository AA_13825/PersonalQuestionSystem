$(
    function () {
    var arr=location.search.split('&');
       var problemId=arr[1].replace('problemId=','');//获取问题编号
        //初始化问题内容
        $.post({
            url:"/select_id",data:{id:problemId},
            success:function (data) {
               $(".questionName").text(""+data.name+"");
               $(".questionRemark").text("问题详述:"+data.problem_remark+"");
               $(".askman").text("提问者:"+data.askman+"");
               $(".questionCreatetime").text("发布于:"+data.create_time+"");
            }
        });
        //初始化回答内容
        $.post({
            url:"/selectAReplyinfo",data: {id:problemId},
            success:function (data) {
                console.log(data);
                for(let i=0;i<data.length;i++)
            {text=" <div class=\"col-lg-12 personalReplyMain\"  >\n" +
                "            <div class=\"row\" style=\"background:#2aabd2\">\n" +
                "                <div class=\"col-lg-4 personalShow\" >\n" +
                "                    <div class=\"btn-taken\"><button class=\"center-block touxiang\" ></button></div>\n" +
                "                    <div class=\"messageShow\" >\n" +
                "                        <p><b class=\"answerman\" >"+data[i].answerman+"</b><span><button class=\"guanzhu\"  id='guanzhu_"+data[i].username+"' onclick='guanzhu(this.id)'>关注</button></span></p>\n" +
                "                        <p><b class=\"answerqianming\" >"+data[i].qianming+"</b></p>\n" +
                "                    </div>\n" +
                "                </div>\n" +
                "            </div>\n" +
                "            <div class=\"row\" style=\"background: lightskyblue\">\n" +
                "                <div class=\"col-lg-10 mainAnswer center-block\" >\n" +
                "                    <p class=\"replymain\">"+data[i].reply_text+"</p>\n" +
                "                    <p  class=\"answertime\" >发布于:"+data[i].createtime+"</p>\n" +
                "                    <div class=\"col-lg-12 dianzanTaken\" >\n" +
                "                        <button  class=\"dianzan\" id=\"dianzan_"+data[i].username+"\" onclick='dianzan(this.id)'>"+data[i].praisecount+"</button>\n" +
                "                    </div>\n" +
                "                </div>\n" +
                "            </div>\n" +
                "        </div>";
                 $(".AllReplyMain").append(text);
            }
            }
        });

    }
);

function inputOpen() { //打开输入
    $(".openAclose").css({"height":'200px',width:'400px'});
    $(".AllReplyMain").css("height",'300px');

}
function inputClose() {//关闭输入
    $(".openAclose").css({"height":'30px',width:'100px'});
    $(".AllReplyMain").css("height",'500px');
}
function sendquestion() {//回答问题
    var arr=location.search.split('&');
    var username=arr[0].replace('?username=','');
    var problemId=arr[1].replace('problemId=','');
    var replyText=$("#questionText").val();
    //查看是否被禁言
    $.post({url:'/selectpersoninfobyuid',data:{uid:username},
    success:function (data) {
        if(data.cantalk==='1')//未被禁言
        {
            $.post({ //查看是否回答过
                url:'/checkreply',data:{username:username,problemId:problemId},
                success(data)
                {
                    if(data===1)//回答过
                    {
                        if(window.confirm("您已经回答过了，要修改答案吗？"))
                        {//修改原答案
                            $.post({
                                url:'/updatereplyText',data:{username:username,problemId:problemId,replyText:replyText},
                                success:function () {
                                    alert("修改成功!");
                                    location.reload();
                                }
                            })
                        }
                    }
                    else //没回答过,直接新增
                    {
                        $.post(
                            {
                                url:'/insertReplyContext',data:{username:username,problemId:problemId,replyText:replyText},
                                success:function () {
                                    alert("回答成功！感谢您的踊跃参与");
                                    location.reload();
                                }
                            }
                        )
                    }
                }
            });
        }
        else
        {
            alert("您已被禁言，无法参与回答！");
        }
    }});
}
function guanzhu(id) {
    var arr=location.search.split('&');
    var username=arr[0].replace('?username=','');//操作用户
    var usernamez=id.replace('guanzhu_','');//目标用户
    if(username===usernamez)
    {
        alert("不能关注自己!")
    }
    else {
    $.post({
        url:'/existSandf',data:{star:usernamez,fun:username},
        success:function (data) {
          if(data===0)//不存在关注
          {
              $.post({
                  url:'/addSandf',data:{star:usernamez,fun:username}, //新增一个关注
                success:function () {
                 alert("关注成功，希望ta可以给你带来优质的内容！");
                }
              })
          }
          else //已经关注过了
          {
              if(window.confirm("您已经关注过该用户了，要取关吗？"))
              {
                $.post(
                    {
                        url:'/delSandf',data:{star:usernamez,fun:username},success:function () {
                           alert("取关成功！");
                        }
                    }
                )
              }
          }
        }
    })
    }
}