$(function () {
   $.post({url:'/selectAlluseCandR',success:function (data) {
        for(let i=0;i<data.length;i++)
        {
            var text="<tr><td class=\"col-lg-3 col-xs-3\" style=\"text-align: center\">"+data[i].username+"</td>\n";
            if (data[i].userright==='3')//封号的情况下
            {
                text+=" <td class=\"col-lg-3 col-xs-3\" style=\"text-align: center\">封号中</td>\n";
                if (data[i].cantalk==='1')//未被禁言
                {
                    text+= "            <td class=\"col-lg-3 col-xs-3\" style=\"text-align: center\"><button class=\"btn-danger\" id='btnz"+data[i].username+"' onclick=\"jinyan(this.id)\">禁言</button></td>"+
                        "            <td class=\"col-lg-3 col-xs-3\" style=\"text-align: center\"><button class=\"btn-success\" id='btn"+data[i].username+"' onclick=\"disfeng(this.id)\">解除封号</button></td></tr>";
                    $("#showData").append(text);
                }
                else//被禁言
                {
                    text+= "            <td class=\"col-lg-3 col-xs-3\" style=\"text-align: center\"><button class=\"btn-success\" id='btnz"+data[i].username+"' onclick=\"disjinyan(this.id)\">解除禁言</button></td>"+
                        "            <td class=\"col-lg-3 col-xs-3\" style=\"text-align: center\"><button class=\"btn-success\" id='btn"+data[i].username+"' onclick=\"disfeng(this.id)\">解除封号</button></td></tr>";
                    $("#showData").append(text);
                }
            }
            else//未被封号
            {
                if (data[i].cantalk==='1')//未被禁言
                {
                    text+="            <td class=\"col-lg-3 col-xs-3\" style=\"text-align: center\">正常</td>\n" +
                    "            <td class=\"col-lg-3 col-xs-3\" style=\"text-align: center\"><button class=\"btn-danger\" id='btnz"+data[i].username+"' onclick=\"jinyan(this.id)\">禁言</button></td>"+
                    "            <td class=\"col-lg-3 col-xs-3\" style=\"text-align: center\"><button class=\"btn-danger\" id='btn"+data[i].username+"' onclick=\"feng(this.id)\">封号</button></td></tr>";
                    $("#showData").append(text);
                }
                else//被禁言
                {
                   text+= "            <td class=\"col-lg-3 col-xs-3\" style=\"text-align: center\">禁言中</td>\n" +
                    "            <td class=\"col-lg-3 col-xs-3\" style=\"text-align: center\"><button class=\"btn-success\" id='btnz"+data[i].username+"' onclick=\"disjinyan(this.id)\">解除禁言</button></td>"+
                    "            <td class=\"col-lg-3 col-xs-3\" style=\"text-align: center\"><button class=\"btn-danger\" id='btn"+data[i].username+"' onclick=\"feng(this.id)\">封号</button></td></tr>";
                    $("#showData").append(text);
                }
            }
        }
       }});
});
function feng(id) {//封号操作
    var username=id.replace('btn','');
    $.post({
        url: '/updateright',data:{username:username,userrights:'3'}
    });
    location.reload();
}
function disfeng(id) {//解封操作
    var username=id.replace('btn','');
    $.post({
        url: '/updateright',data:{username:username,userrights:'1'}
    });
    location.reload();
}
function jinyan(id) { //禁言操作
    var username=id.replace('btnz','');
    $.post({
        url: '/updatecantalk',data:{username:username,cantalk:'0'}
    });
    location.reload();
}

function disjinyan(id) { //解除禁言操作
    var username=id.replace('btnz','');
    $.post({
        url: '/updatecantalk',data:{username:username,cantalk:'1'}
    });
    location.reload();
}