$(function () {
        $.post(
            {url:'/select_unsolve',
                success:
                    function (data) {
                        console.log(data);
                        for (var i=0;i<data.length;i++)
                        {  var obj=$("#problemList");
                            var text= " <button class=\"promblemlist_btn\" id=\"problem"+data[i].id+"\" onclick=\"changetable(this.id)\">"+data[i].id+""+data[i].name+"</button>";
                            obj.append(text);
                        }
                    }
            }
        );
    }
);

function changetable(id) {//未解决的查询
    var save_btn=$("#save_btn");
    save_btn.attr("disabled","true");//保存一开始不可用
    var solve_btn=$("#solve_btn");
    solve_btn.removeAttr("disabled","true");//提交可用
    var id_z=id.replace('problem','');//替换固定字符
    //问题基本
    var obj1=$("#problemBase");
    obj1.children(".problembase_z").remove();
    //问题简述
    var  obj2=$("#problemRemark");
    obj2.children(".problemRemark_z").remove();
    //问题的解决方法
    var obj3=$("#solveWay");
    obj3.children(".solveWay_z").remove();
    $.post({
        url:'/select_id',data:{id:parseInt(id_z)},
        success:function (data) {
            var text1=" <tr class='problembase_z'><td><input type=\"text\"   value=\""+data.id+"\" id=\"sqlid\" readonly>\n</td>" +
                "           <td> <input type=\"text\"   value=\""+data.name+"\" placeholder=\"请输入名称\"  id=\"sqlname\" readonly ondblclick=\"canwrite(this.id)\">\n</td>" +
                "            <td><input type=\"text\"   value=\""+data.redu+"\" placeholder=\"请输入热度\"  id=\"sqltype\" readonly ondblclick=\"canwrite(this.id)\">\n</td>" +
                "            <td><input type=\"text\"  value=\""+data.askman+"\"  placeholder=\"请输入申请人\" id=\"sqlaskman\" readonly ondblclick=\"canwrite(this.id)\">\n</td>" +
                "            <td><input type=\"text\"   value=\""+data.create_time+"\" id=\"sqlcreatetime\" readonly></td></tr>";
            obj1.append(text1);
            var text2="<p class=\"problemRemark_z\"><textarea  style=\"width:400px; height:200px;\"  maxlength=\"255\" placeholder=\"请输入简介\"  id=\"sqlremark\" readonly ondblclick=\"canwrite(this.id)\">"+data.problem_remark+"</textarea></p>";
            obj2.append(text2);
            var text3="<p class=\"solveWay_z\"><textarea  style=\"width:400px; height:200px;\"  maxlength=\"255\" placeholder=\"请输入删除问题的原因\"  id=\"sqlsolveway\" readonly ondblclick=\"canwrite(this.id)\">"+data.solveway+"</textarea></p>";
            obj3.append(text3);
        }
    });
}

function canwrite(id) { //进行编辑后的事件

    var obj=$("#"+id+"");
    obj.val('');
    obj.removeAttr("readonly");
    //解除保存按钮不可选的事件
    var obj_save=$("#save_btn");
    obj_save.removeAttr("disabled");
}

function changeisolvetable() {       //搜索改已解决列表
    var obj=$("#select_issolve");//获取查询框内容
    var a=obj.val();
    var b="%"+a+"%";//转化为通配串
    var obj_listbody=$("#solveList");//添加用
    $.post({
        url:"/select_issolve",data: {namelike:b},
        success:function (data) {
            if(data.length===0)
            {
                alert("没有找到匹配的结果哦！")
            }
            else //找到结果后改变布局
            {
                obj_listbody.children(".solveList_btn").remove();
                for(var i=0;i<data.length;i++)
                {
                    var text= " <button class=\"solveList_btn\" id=\"problem"+data[i].id+"\" onclick=\"changetable_z(this.id)\">"+data[i].id+""+data[i].name+"</button>";
                    obj_listbody.append(text);
                }
            }
        }
    });
}
function  changetable_z(id) { //点击已解决列表的事件
    var save_btn=$("#save_btn");
    save_btn.attr("disabled","true");//保存不可用
    var solve_btn=$("#solve_btn");
    solve_btn.attr("disabled","true");//提交不可用
    var id_z=id.replace('problem','');//替换固定字符
    //问题基本
    var obj1=$("#problemBase");
    obj1.children(".problembase_z").remove();
    //问题简述
    var  obj2=$("#problemRemark");
    obj2.children(".problemRemark_z").remove();
    //问题的解决方法
    var obj3=$("#solveWay");
    obj3.children(".solveWay_z").remove();
    $.post({
        url:'/select_id',data:{id:parseInt(id_z)},
        success:function (data) {
            console.log(data);
            var text1=" <tr class='problembase_z'><td><input type=\"text\"   value=\""+data.id+"\" id=\"sqlid\" readonly>\n</td>" +
                "           <td> <input type=\"text\"   value=\""+data.name+"\" placeholder=\"请输入名称\"  id=\"sqlname\" readonly>\n</td>" +
                "            <td><input type=\"text\"   value=\""+data.redu+"\" placeholder=\"请输入类型\"  id=\"sqltype\" readonly>\n</td>" +
                "            <td><input type=\"text\"  value=\""+data.askman+"\"  placeholder=\"请输入申请人\" id=\"sqlaskman\" readonly >\n</td>" +
                "            <td><input type=\"text\"   value=\""+data.create_time+"\" id=\"sqlcreatetime\" readonly></td></tr>";
            obj1.append(text1);
            var text2="<p class=\"problemRemark_z\"><textarea  style=\"width:400px; height:200px;\"  maxlength=\"255\" placeholder=\"请输入简介\"  id=\"sqlremark\" readonly >"+data.problem_remark+"</textarea></p>";
            obj2.append(text2);
            var text3="<p class=\"solveWay_z\"><textarea  style=\"width:400px; height:200px;\"  maxlength=\"255\" placeholder=\"请输入解决方式\"  id=\"sqlsolveway\" readonly >"+data.solveway+"</textarea></p>";
            obj3.append(text3);
        }
    });
}
//保存方法
function save() {
//取4个可以修改的值：名字 申请人 类型 详述
    var id=$("#sqlid").val();
    var name=$("#sqlname").val();
    var type=$("#sqltype").val();
    var askman=$("#sqlaskman").val();
    var remark=$("#sqlremark").val();
    var save_btn=$("#save_btn");
    save_btn.attr("disabled","true");
    $.post({
        url:'update_unsolve',data:{id:parseInt(id),name:name,redu:parseInt(type),remark:remark,askman:askman},
        success:function () {
            alert(id+"事件修改成功!");
        }
    })
}
//提交方法
function tosolve() {
//判断值是否为空
    var id=$("#sqlid").val();
    var name=$("#sqlname").val();
    var type=$("#sqltype").val();
    var askman=$("#sqlaskman").val();
    var remark=$("#sqlremark").val();
    var solveway=$("#sqlsolveway").val();
    if(solveway==="null"||solveway==='')
    {
        alert("解决方法为空,不能提交解决方案！");
    }
    else {
        if (name===""||type===""||askman===""||remark==="")
        {alert("有数据没有完善，不能提交解决方案!")}
        else
        {
            $.post({
                url:'gotosolve',data:{id:parseInt(id),solveway:solveway},success:function () {

                    alert("事件"+id+"已提交至解决方案");
                    var save_btn=$("#save_btn");
                    save_btn.attr("disabled","true");//保存不可用
                    var solve_btn=$("#solve_btn");
                    solve_btn.attr("disabled","true");//提交不可用
                    $("#problemList").children("#problem"+id+"").remove();
                    //移除显示的项目
                    //问题基本
                    var obj1=$("#problemBase");
                    obj1.children(".problembase_z").remove();
                    //问题简述
                    var  obj2=$("#problemRemark");
                    obj2.children(".problemRemark_z").remove();
                    //问题的解决方法
                    var obj3=$("#solveWay");
                    obj3.children(".solveWay_z").remove();
                }
            });
        }
    }
}

