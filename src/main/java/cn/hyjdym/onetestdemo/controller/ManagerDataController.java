package cn.hyjdym.onetestdemo.controller;

import cn.hyjdym.onetestdemo.Service.*;
import cn.hyjdym.onetestdemo.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
public class ManagerDataController {
    @Autowired
   solveService solveService;
   @Autowired
   replyinfoService replyinfoService;
   @Autowired
    personalService personalService;
   @Autowired
    sandfService sandfService;
   @Autowired
    interinfoService interinfoService;
   @Autowired
    userzService userzService;
   //根据字段查询已经解决问题
    @ResponseBody
    @RequestMapping("select_issolve")
    public List<solve> select_issolve(String namelike)
    {
        return solveService.select_issolve(namelike);
    }

    //根据字段查询未解决问题
    @ResponseBody
    @RequestMapping("select_unsolveByQuestionLike")
    public List<solve> select_unsolveByQuestionLike(String questionLike)
    {
        return  solveService.select_unsolveByQuestionLike(questionLike);
    }

    //增加一个问题
    @ResponseBody
    @RequestMapping("/addnewProblem")
    public void addnewProblem(String askman,String problemName,String problemRemark)
    {
        solveService.addnewProblem(askman,problemName,problemRemark);
    }
    //查询所有未解决问题

    @ResponseBody
    @RequestMapping("/select_unsolve")
    public List<solve> select_unsolve()
    {

        return  solveService.select_unsolve();
    }

    @ResponseBody//按照热度查询所有的数据
    @RequestMapping("/select_unsolveOrderByredu")
    public List<solve> select_unsolveOrderByredu()
    {
        return solveService.select_unsolveOrderByredu();
    }

    //对点击问题进行查询(就是根据id查询拉！)
    @ResponseBody
    @RequestMapping("/select_id")
    public solve select_id(int id)
    {
        return  solveService.select_id(id);
    }
    //更改问题信息
    @ResponseBody
    @RequestMapping("/update_unsolve")
    public void update_unsolve(int id,String name,String askman,String remark,Integer redu)
    {
        solveService.update_unsolve(id,name,askman,remark,redu);
    }
    //提交至解决问题栏目
    @ResponseBody
    @RequestMapping("/gotosolve")
    public  void  gotosolve(int id,String solveway)
    {
        solveService.gotosolve(id,solveway);
    }


//replyinfo表的操作
    @ResponseBody
    @RequestMapping("/selectReplyByProblemId")//根据问题id找所有回答
    public List<replyinfo> selectReplyByProblemId(String id)
    {
     return replyinfoService.selectReplyByProblemId(id);
    }

    @ResponseBody
    @RequestMapping("/countuseranswer")//返回一个用户回答的数量
    public String countuseranswer(String uid)
    {
        return Integer.toString(replyinfoService.countuseranswer(uid));
    }

    @ResponseBody
    @RequestMapping("/selectAReplyinfo")
    public List<replyinfo_z> selectAReplyinfo(String id)//获取一整条记录
    {
       List<replyinfo_z> replyinfo_zs=new ArrayList<>();
       List<replyinfo> replyinfos=replyinfoService.selectReplyByProblemId(id);//获取每一条replyinfo
        //进行封装3
      for(int i=0;i<replyinfos.size();i++)
      {
       replyinfo_z replyinfo_z=new replyinfo_z();
        //根据用户编号查询用户信息表
        personalinfo personalinfo=personalService.selectpersoninfobyuid(replyinfos.get(i).getReply_user());
        replyinfo_z.setUsername(personalinfo.getUser());//获取用户名
        replyinfo_z.setAnswerman(personalinfo.getName());//获取用户昵称
          replyinfo_z.setQianming(personalinfo.getQianming());//获取签名
          replyinfo_z.setPraisecount(replyinfos.get(i).getPraisecount());
          replyinfo_z.setCreatetime(replyinfos.get(i).getCreatetime());
          replyinfo_z.setReply_text(replyinfos.get(i).getReply_text());
          replyinfo_zs.add(replyinfo_z);
      }
      return replyinfo_zs;
    }
    //查询用户是否在某一个问题下回答过
    @ResponseBody
    @RequestMapping("/checkreply")
    public int checkreply(String username,String problemId)
    {
        return  replyinfoService.checkreply(username,problemId);
    }
    //修改原答案
    @ResponseBody
    @RequestMapping("/updatereplyText")
    public void updatereplyText(String username,String problemId,String replyText)
    {
        replyinfoService.updatereplyText(username,problemId,replyText);
    }
    //增加一条回答内容
    @ResponseBody
    @RequestMapping("/insertReplyContext")
    public void insertReplyContext(String username,String problemId,String replyText)
    {
       replyinfoService.insertReplyContext(username,problemId,replyText);
    }

 //用户信息表的操作
    @ResponseBody
    @RequestMapping("/selectpersoninfobyuid")//根据用户编号寻找用户登记的信息
         public personalinfo selectpersoninfobyuid(String uid)
  {
      return personalService.selectpersoninfobyuid(uid);
  }
@ResponseBody
@RequestMapping("selectnamebyusername") //查询username的用户是否登记了个人昵称
public String selectnameebtusername(String username)
{
   return personalService.selectnamebyusername(username);
}

@ResponseBody
@RequestMapping("/addonepersoninfo")//增加一条个人信息
public void addonepersoninfo(String username,String name,String qianming,String profession,String favor)
{
    personalService.addonepersoninfo(username,name,qianming,profession,favor);
}
@ResponseBody
  @RequestMapping("/selectnamecount")//根据昵称查询用户编号数量(检测是否重名)
  public Integer selectnamecount(String name)
  {
      return personalService.selectnamecount(name);
  }
  @ResponseBody
  @RequestMapping("/updateforth")//更改基本四项
  public void updateforth(String username, String name,String favor,String qianming,String profession)
  {
  personalService.updateforth(username,name,favor,qianming,profession);
  }
 @ResponseBody
 @RequestMapping("/updatecantalk")//更改用户禁言状态
 public void updatecantalk(String username,String cantalk)
 {
     personalService.updatecantalk(username,cantalk);
 }
  @ResponseBody
   @RequestMapping("/selectApersonalMess")
  public personal_z selectApersonalMess(String uid)//根据用户编号查询用户的所有相关信息
  {
  personal_z personal_z=new personal_z();
  personalinfo personalinfo=personalService.selectpersoninfobyuid(uid);
  //获取用户等级的基本四项
  personal_z.setName(personalinfo.getName());
  personal_z.setFavor(personalinfo.getFavor());
  personal_z.setQinaming(personalinfo.getQianming());
  personal_z.setProfession(personalinfo.getProfession());
  //获取粉丝交互表两项
  personal_z.setFun(Integer.toString(sandfService.tobefunbyuid(uid)));//关注数量
  personal_z.setStar(Integer.toString(sandfService.tobestarbyuid(uid)));//粉丝数量
      //回答表一项
    personal_z.setAnswercount(Integer.toString(replyinfoService.countuseranswer(uid)));
      //点赞交互表一项
      personal_z.setPraisecount(Integer.toString(interinfoService.counttotalparise(uid)));
      return personal_z;
  }
  @ResponseBody
  @RequestMapping("/selectAlluseCandR")//返回所有登记用户的账号状态
  public List<userbasestatus> selectAlluseCandR()
  {   List<userbasestatus> userbasestatusList=new ArrayList<>();
      List<personalinfo> personalinfoList=personalService.seletAlluser();//
      for(int i=0;i<personalinfoList.size();i++)
      {  userbasestatus userbasestatus1=new userbasestatus();
         user user1= userzService.selectOneuse(personalinfoList.get(i).getUser());//查询一个用户
          userbasestatus1.setUsername(personalinfoList.get(i).getUser());//获取用户名
          userbasestatus1.setCantalk(personalinfoList.get(i).getCantalk());//是否禁言
          userbasestatus1.setUserright(user1.getUserrights());
          userbasestatusList.add(userbasestatus1);
      }
      return userbasestatusList;
  }
   //返回一个用户的所有(关注/被粉的用户信息)
    @ResponseBody
    @RequestMapping("/selectInterPerson")
   public List<personalinfo> selectInterPerson(String username,String type)
   {
       List<personalinfo> personalinfoList=new ArrayList<>();
       if (type.equals("fun"))//查找关注的人
       {
           List<sandf> sandfs=sandfService.selectLikeAfun(username);//寻找所有关注信息
           for(int i=0;i<sandfs.size();i++)
           {
               personalinfo personalinfo=personalService.selectpersoninfobyuid(sandfs.get(i).getStar_user());
               personalinfoList.add(personalinfo);
           }
           return personalinfoList;
       }
       else if (type.equals("star"))//查找粉丝
       {
           List<sandf> sandfs=sandfService.selectLikeAStar(username);//寻找所有粉丝信息
           for(int i=0;i<sandfs.size();i++)
           {
               personalinfo personalinfo=personalService.selectpersoninfobyuid(sandfs.get(i).getFun_user());
               personalinfoList.add(personalinfo);
           }
           return personalinfoList;
       }
       return personalinfoList;
   }

//关注表的操作
     @ResponseBody
    @RequestMapping("/tobefunbyuid")//根据用户编号寻找关注的数量
   public String tobefunbyuid(String uid)
{
    return Integer.toString(sandfService.tobefunbyuid(uid));
}
    @ResponseBody
    @RequestMapping("/tobestarbyuid")//根据用户编号寻找受关注的数量
    public String tobestarbyuid(String uid)
    {
        return Integer.toString(sandfService.tobestarbyuid(uid));
    }

    @ResponseBody
    @RequestMapping("/existSandf")//查看用户是否关注过目标用户
    public int existSandf(String star,String fun)
    {
        return sandfService.existSandf(star,fun);
    }

    @ResponseBody
    @RequestMapping("/addSandf")//关注操作
    public void addSandf(String star,String fun)
    {
      sandfService.addSandf(star,fun);
    }

    @ResponseBody
    @RequestMapping("/delSandf")//取关操作
    public void delSandf(String star,String fun)
    {
       sandfService.delSandf(star,fun);
    }
//赞同表的操作
    @ResponseBody
    @RequestMapping("/counttotalparise")//获取一个用户的总赞数
public  String  counttotalparise(String uid)
{
    return Integer.toString(interinfoService.counttotalparise(uid));
}

//关于用户表的操作
    //查询所有用户
    @ResponseBody
    @RequestMapping("/selectAlluse") //查询所有身份是用户的
    public List<user> selectAlluse()
    {
        return userzService.selectAlluse();
    }

    @ResponseBody
    @RequestMapping("/updateright")//更改用户的登录状态
    public void updateright(String username,String userrights)
    {
        userzService.updateright(username,userrights);
    }
}
