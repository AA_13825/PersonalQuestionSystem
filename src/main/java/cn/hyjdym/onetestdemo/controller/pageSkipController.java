package cn.hyjdym.onetestdemo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class pageSkipController {
    //通用
    @RequestMapping("/personalMessage")// 个人信息
    public String personalMessage()
    {
        return "common/personalMessage";
    }
    //用户用
    @RequestMapping("/questionGround")// 问题广场
    public String questionGround()
    {
        return "user/questionGround";
    }
    @RequestMapping("/friendList")
    public String friendList()
    {return "user/friendList";}
    @RequestMapping("/AquestionMain")
    public String AquestionMain(){return "user/AquestionMain";}
   //管理员用
   @RequestMapping("/tecsolve")//问题列表
   public String tecsolve()
   {
       return "manager/solve";
   }
   @RequestMapping("/managerUse")//管理员跳转到用户界面
    public String managerUse(){return "manager/manager_use";}

}

