package cn.hyjdym.onetestdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

@Component
@SpringBootApplication
public class OnetestdemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(OnetestdemoApplication.class, args);
    }

}
