package cn.hyjdym.onetestdemo.ServiceImpl;

import cn.hyjdym.onetestdemo.Dao.userDao;
import cn.hyjdym.onetestdemo.Service.userzService;
import cn.hyjdym.onetestdemo.entity.user;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class userzServiceImpl implements userzService {
    @Autowired
    userDao userDao;
    @Override
    public List<user> selectAlluse() {
        return userDao.selectAlluse();
    }

    @Override
    public user selectOneuse(String user) {
        return userDao.selectOneuse(user);
    }

    @Override
    public void updateright(String username, String userrights) {
        userDao.updateright(username,userrights);
    }
}
