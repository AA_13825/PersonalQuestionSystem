package cn.hyjdym.onetestdemo.ServiceImpl;

import cn.hyjdym.onetestdemo.Dao.personalDao;
import cn.hyjdym.onetestdemo.Service.personalService;
import cn.hyjdym.onetestdemo.entity.personalinfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class personalServiceImpl implements personalService {
    @Autowired
    personalDao personalDao;
    @Override
    public personalinfo selectpersoninfobyuid(String uid) {
        return personalDao.selectpersoninfobyuid(uid);
    }

    @Override
    public Integer selectnamecount(String name) {
        return personalDao.selectnamecount(name);
    }

    @Override
    public void updateforth(String username, String name, String favor, String qianming, String profession) {
        personalDao.updateforth(username,name,favor,qianming,profession);
    }

    @Override
    public String selectnamebyusername(String username) {

        return personalDao.selectnamebyusername(username);
    }

    @Override
    public void addonepersoninfo(String username, String name, String qianming, String profession, String favor) {
        personalDao.addonepersoninfo(username,name,qianming,profession,favor);
    }

    @Override
    public List<personalinfo> seletAlluser() {
        return personalDao.selectAlluser();
    }

    @Override
    public void updatecantalk(String username, String cantalk) {
        personalDao.updatecantalk(username,cantalk);
    }
}
