package cn.hyjdym.onetestdemo.ServiceImpl;


import cn.hyjdym.onetestdemo.Service.UserService;
import cn.hyjdym.onetestdemo.model.User;
import cn.hyjdym.onetestdemo.repository.UserRepository;
import cn.hyjdym.onetestdemo.util.Comm;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UserRepository userRepository;
    @Override
    public User findbyId(String id) {
        return userRepository.findById(id).orElse(null);
    }
    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }
    @Override
    public User save(User aUser) {
        User vUser=userRepository.findByUsername((aUser.getUsername().toString()));
        if(vUser == null){
            aUser.setRolename("1");
            aUser.setRolenumber(1);
            aUser.setRoledescription("普通注册");
            //生成随机码，用于验证邮箱
            aUser.setUsercheck(Comm.getRandom(8));
            return userRepository.save(aUser);
        }else{
            return null;
        }

    }
    @Override
    public void delete(String id) {
        userRepository.deleteById(id);
    }

    @Override
    public int verifyLogin(User user){
        User gUser= userRepository.findByUsername((user.getUsername().toString()));
        if(gUser!=null){
            if(gUser.getUserpassword().equals(user.getUserpassword()) && Integer.parseInt(gUser.getUserrights())>0){
                return Integer.parseInt(gUser.getUserrights());
            }else{
                return 0;
            }
        }else {
            return 0;
        }
    }

    @Override
    public boolean verifyEmail(User user){
        User gUser= userRepository.findByUsername((user.getUsername().toString()));
        if(gUser!=null){
            if(gUser.getUsercheck().equals(user.getUsercheck())){
                gUser.setUsercheck("");
                gUser.setRoledescription(gUser.getRoledescription()+"->邮箱验证成功");
                userRepository.save(gUser);
                return true;
            }else{
                return false;
            }
        }else {
            return false;
        }
    }

}