package cn.hyjdym.onetestdemo.ServiceImpl;

import cn.hyjdym.onetestdemo.Dao.replyinfoDao;
import cn.hyjdym.onetestdemo.Service.replyinfoService;
import cn.hyjdym.onetestdemo.entity.replyinfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Service
public class replyinfoServiceImpl implements replyinfoService {
    @Autowired
    replyinfoDao replyinfoDao;
    @Override
    public List<replyinfo> selectReplyByProblemId(String id) {
        return replyinfoDao.selectReplyByProblemId(id);
    }

    @Override
    public int countuseranswer(String uid) {
        return replyinfoDao.countuseranswer(uid);
    }

    @Override
    public int checkreply(String username, String problemId) {
        return replyinfoDao.checkreply(username,problemId);
    }

    @Override
    public void updatereplyText(String username, String problemId, String replyText) {
        replyinfoDao.updatereplyText(username,problemId,replyText);
    }

    @Override
    public void insertReplyContext(String username, String problemId, String replyText) {
        replyinfoDao.insertReplyContext(username,problemId,replyText);
    }
}
