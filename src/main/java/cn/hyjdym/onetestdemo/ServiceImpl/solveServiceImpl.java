package cn.hyjdym.onetestdemo.ServiceImpl;

import cn.hyjdym.onetestdemo.Dao.solveDao;
import cn.hyjdym.onetestdemo.Service.solveService;
import cn.hyjdym.onetestdemo.entity.solve;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class solveServiceImpl implements solveService {
@Autowired
solveDao solveDao;
    @Override
    public List<solve> select_unsolve() {
        return solveDao.selectunsolve();
    }

    @Override
    public solve select_id(int id) {
        return solveDao.select_id(id);
    }

    @Override
    public List<solve> select_issolve(String namelike) {
        return solveDao.select_issolve(namelike);
    }

    @Override
    public void update_unsolve(int id, String name, String askman, String remark, Integer redu) {
        solveDao.update_unsolve(id,name,askman,remark,redu);
    }

    @Override
    public void gotosolve(int id, String solveway) {
        solveDao.gotosolve(id,solveway);
    }

    @Override
    public void addnewProblem(String askman, String problemName, String problemRemark) {
        solveDao.addnewProblem(askman,problemName,problemRemark);
    }

    @Override
    public List<solve> select_unsolveByQuestionLike(String questionLike) {
        return solveDao.select_unsolveByQuestionLike(questionLike);
    }

    @Override
    public List<solve> select_unsolveOrderByredu() {
        return solveDao.select_unsolveOrderByredu();
    }
}
