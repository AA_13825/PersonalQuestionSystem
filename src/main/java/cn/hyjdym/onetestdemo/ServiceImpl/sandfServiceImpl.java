package cn.hyjdym.onetestdemo.ServiceImpl;

import cn.hyjdym.onetestdemo.Dao.sandfDao;
import cn.hyjdym.onetestdemo.Service.sandfService;
import cn.hyjdym.onetestdemo.entity.sandf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class sandfServiceImpl implements sandfService {
    @Autowired
    sandfDao sandfDao;

    @Override
    public int tobefunbyuid(String uid) {
        return sandfDao.tobefunbyuid(uid);
    }

    @Override
    public int tobestarbyuid(String uid) {
        return sandfDao.tobestarbyuid(uid);
    }

    @Override
    public int existSandf(String star, String fun) {
        return sandfDao.existSandf(star,fun);
    }

    @Override
    public void addSandf(String star, String fun) {
        sandfDao.addSandf(star,fun);
    }

    @Override
    public void delSandf(String star, String fun) {
        sandfDao.delSandf(star,fun);
    }

    @Override
    public List<sandf> selectLikeAfun(String username) {
        return sandfDao.selectLikeAfun(username);
    }

    @Override
    public List<sandf> selectLikeAStar(String username) {
        return sandfDao.selectLikeAStar(username);
    }
}
