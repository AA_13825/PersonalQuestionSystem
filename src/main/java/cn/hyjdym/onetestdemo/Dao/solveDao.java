package cn.hyjdym.onetestdemo.Dao;

import cn.hyjdym.onetestdemo.entity.solve;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface solveDao {
    List<solve> selectunsolve();

    solve select_id(@Param("id") int id);

    List<solve> select_issolve(@Param("namelike") String namelike);

    void update_unsolve(@Param("id") int id, @Param("name") String name,@Param("askman") String askman,@Param("remark") String remark, @Param("redu") Integer redu);

    void gotosolve(@Param("id") int id,@Param("solveway") String solveway);

    void addnewProblem(@Param("askman") String askman, @Param("problemName") String problemName, @Param("problemRemark") String problemRemark);

    List<solve> select_unsolveByQuestionLike(@Param("questionLike") String questionLike);

    List<solve> select_unsolveOrderByredu();
}
