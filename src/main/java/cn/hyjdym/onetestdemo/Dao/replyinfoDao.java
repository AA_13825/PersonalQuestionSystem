package cn.hyjdym.onetestdemo.Dao;

import cn.hyjdym.onetestdemo.entity.replyinfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface replyinfoDao {
    List<replyinfo> selectReplyByProblemId(@Param("id") String id);

    int countuseranswer(@Param("uid") String uid);

    int checkreply(@Param("username") String username,@Param("problemId") String problemId);

    void updatereplyText(@Param("username") String username,@Param("problemId") String problemId,@Param("replyText") String replyText);

    void insertReplyContext(@Param("username") String username,@Param("problemId") String problemId, @Param("replyText") String replyText);
}
