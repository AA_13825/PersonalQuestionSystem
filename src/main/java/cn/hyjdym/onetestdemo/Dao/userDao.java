package cn.hyjdym.onetestdemo.Dao;

import cn.hyjdym.onetestdemo.entity.user;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@Mapper
public interface userDao {
    List<user> selectAlluse();

    user selectOneuse(@Param("username") String user);

    void updateright(@Param("username") String username, @Param("userrights") String userrights);
}
