package cn.hyjdym.onetestdemo.Dao;

import cn.hyjdym.onetestdemo.entity.personalinfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface personalDao {
    personalinfo selectpersoninfobyuid(@Param("uid") String uid);

    Integer selectnamecount(@Param("name") String name);

    void updateforth(@Param("username") String username, @Param("name") String name, @Param("favor") String favor, @Param("qianming") String qianming, @Param("profession") String profession);

    String selectnamebyusername(@Param("username") String username);

    void addonepersoninfo(@Param("username") String username, @Param("name") String name, @Param("qianming") String qianming, @Param("profession") String profession, @Param("favor") String favor);

    List<personalinfo> selectAlluser();

    void updatecantalk(@Param("username") String username, @Param("cantalk") String cantalk);
}
