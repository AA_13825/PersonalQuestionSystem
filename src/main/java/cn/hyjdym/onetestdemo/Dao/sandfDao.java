package cn.hyjdym.onetestdemo.Dao;

import cn.hyjdym.onetestdemo.entity.sandf;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface sandfDao {
    int tobefunbyuid(@Param("uid") String uid);

    int tobestarbyuid(@Param("uid") String uid);

    int existSandf(@Param("star") String star, @Param("fun") String fun);

    void delSandf(@Param("star") String star, @Param("fun") String fun);

    void addSandf(@Param("star") String star, @Param("fun") String fun);

    List<sandf> selectLikeAfun(@Param("username") String username);

    List<sandf> selectLikeAStar(@Param("username") String username);
}
