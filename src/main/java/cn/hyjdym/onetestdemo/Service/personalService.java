package cn.hyjdym.onetestdemo.Service;

import cn.hyjdym.onetestdemo.entity.personalinfo;

import java.util.List;

public interface personalService {
    personalinfo selectpersoninfobyuid(String uid);

    Integer selectnamecount(String name);

    void updateforth(String username, String name, String favor, String qianming, String profession);

    String selectnamebyusername(String username);

    void addonepersoninfo(String username,String name, String qianming, String profession, String favor);

    List<personalinfo> seletAlluser();

    void updatecantalk(String username, String cantalk);
}
