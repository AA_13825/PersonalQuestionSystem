package cn.hyjdym.onetestdemo.Service;

import cn.hyjdym.onetestdemo.model.User;

import java.util.List;

public interface UserService {
    User findbyId(String id);

    List<User> findAll();

    User save(User ayUser);

    void delete(String id);

    int verifyLogin(User user);

    boolean verifyEmail(User user);



}

