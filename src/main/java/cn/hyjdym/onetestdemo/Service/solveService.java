package cn.hyjdym.onetestdemo.Service;

import cn.hyjdym.onetestdemo.entity.solve;

import java.util.List;

public interface solveService {
    List<solve> select_unsolve();

   solve select_id(int id);


    List<solve> select_issolve(String namelike);

    void update_unsolve(int id, String name, String askman, String remark, Integer redu);

    void gotosolve(int id, String solveway);

    void addnewProblem(String askman, String problemName, String problemRemark);

    List<solve> select_unsolveByQuestionLike(String quetionLike);

    List<solve> select_unsolveOrderByredu();
}