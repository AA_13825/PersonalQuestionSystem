package cn.hyjdym.onetestdemo.Service;

import cn.hyjdym.onetestdemo.entity.sandf;

import java.util.List;

public interface sandfService {
    int tobefunbyuid(String uid);

    int tobestarbyuid(String uid);

    int existSandf(String star, String fun);

    void addSandf(String star, String fun);

    void delSandf(String star, String fun);

    List<sandf> selectLikeAfun(String username);

    List<sandf> selectLikeAStar(String username);
}
