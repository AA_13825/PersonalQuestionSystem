package cn.hyjdym.onetestdemo.Service;

import cn.hyjdym.onetestdemo.entity.replyinfo;

import java.util.List;

public interface replyinfoService {
    List<replyinfo> selectReplyByProblemId(String id);

    int countuseranswer(String uid);

    int checkreply(String username, String problemId);

    void updatereplyText(String username, String problemId, String replyText);

    void insertReplyContext(String username, String problemId, String replyText);
}
