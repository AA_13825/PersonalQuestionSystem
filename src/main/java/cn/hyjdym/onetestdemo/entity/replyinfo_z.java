package cn.hyjdym.onetestdemo.entity;

import java.util.Date;

public class replyinfo_z {//用于存储回答的内容和回答者情况二者
    private String reply_text;
    private Date createtime;
    private int praisecount;
    private String answerman;//回答问题的人名昵称
    private String qianming;//回答问题人的签名
    private String username;//回答问题用户名

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getReply_text() {
        return reply_text;
    }

    public void setReply_text(String reply_text) {
        this.reply_text = reply_text;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public int getPraisecount() {
        return praisecount;
    }

    public void setPraisecount(int praisecount) {
        this.praisecount = praisecount;
    }

    public String getAnswerman() {
        return answerman;
    }

    public void setAnswerman(String answerman) {
        this.answerman = answerman;
    }

    public String getQianming() {
        return qianming;
    }

    public void setQianming(String qianming) {
        this.qianming = qianming;
    }
}
