package cn.hyjdym.onetestdemo.entity;

import java.util.Date;

public class solve {
    private int id;//问题编号
    private String name;//问题名字
    private int redu;//问题类型
    private String askman;//发起问题的人
    private String problem_remark;//问题备注
    private String solveway;//解决方法
    private Date create_time;//创建时间
    private int issolve;//问题是否解决

    public int getRedu() {
        return redu;
    }

    public void setRedu(int redu) {
        this.redu = redu;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getAskman() {
        return askman;
    }

    public void setAskman(String askman) {
        this.askman = askman;
    }

    public String getProblem_remark() {
        return problem_remark;
    }

    public void setProblem_remark(String problem_remark) {
        this.problem_remark = problem_remark;
    }

    public String getSolveway() {
        return solveway;
    }

    public void setSolveway(String solveway) {
        this.solveway = solveway;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public int getIssolve() {
        return issolve;
    }

    public void setIssolve(int issolve) {
        this.issolve = issolve;
    }
}
