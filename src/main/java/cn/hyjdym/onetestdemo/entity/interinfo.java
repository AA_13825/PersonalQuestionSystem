package cn.hyjdym.onetestdemo.entity;

public class interinfo {//点赞交互记录
    private String praised_user;//被赞的人
    private String praise_user;//点赞的人
    private String problem_id;//问题id

    public String getPraised_user() {
        return praised_user;
    }

    public void setPraised_user(String praised_user) {
        this.praised_user = praised_user;
    }

    public String getPraise_user() {
        return praise_user;
    }

    public void setPraise_user(String praise_user) {
        this.praise_user = praise_user;
    }

    public String getProblem_id() {
        return problem_id;
    }

    public void setProblem_id(String problem_id) {
        this.problem_id = problem_id;
    }
}
