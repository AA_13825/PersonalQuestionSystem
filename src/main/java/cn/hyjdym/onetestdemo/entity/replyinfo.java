package cn.hyjdym.onetestdemo.entity;

import java.util.Date;

public class replyinfo {  //统计回答内容和点赞量
    private String promblem_id;//问题编号
    private String reply_text;//回答内容
    private String reply_user;//回答人
    private Date createtime;//回答时间
    private int praisecount;//点赞数

    public String getPromblem_id() {
        return promblem_id;
    }

    public void setPromblem_id(String promblem_id) {
        this.promblem_id = promblem_id;
    }

    public String getReply_text() {
        return reply_text;
    }

    public void setReply_text(String reply_text) {
        this.reply_text = reply_text;
    }

    public String getReply_user() {
        return reply_user;
    }

    public void setReply_user(String reply_user) {
        this.reply_user = reply_user;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public int getPraisecount() {
        return praisecount;
    }

    public void setPraisecount(int praisecount) {
        this.praisecount = praisecount;
    }
}
