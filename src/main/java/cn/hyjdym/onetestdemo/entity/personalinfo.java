package cn.hyjdym.onetestdemo.entity;

import java.util.Date;

public class personalinfo {//个人信息
    private String qianming;//签名
    private String profession;//职业
    private String favor;//爱好
    private String user;//用户名
    private String name;//昵称（不能重复）
    private String cantalk;//是否被禁言（1是禁言 0是没有）
    private Date birthday;//生日

    public String getQianming() {
        return qianming;
    }

    public void setQianming(String qianming) {
        this.qianming = qianming;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getFavor() {
        return favor;
    }

    public void setFavor(String favor) {
        this.favor = favor;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCantalk() {
        return cantalk;
    }

    public void setCantalk(String cantalk) {
        this.cantalk = cantalk;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
}
