package cn.hyjdym.onetestdemo.entity;

public class userbasestatus {//记录用户的两种基本状态,从personalinfo表开始记录
    private String username;
    private String cantalk;//是否被禁言
    private String userright;//是否被封号

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCantalk() {
        return cantalk;
    }

    public void setCantalk(String cantalk) {
        this.cantalk = cantalk;
    }

    public String getUserright() {
        return userright;
    }

    public void setUserright(String userright) {
        this.userright = userright;
    }
}
