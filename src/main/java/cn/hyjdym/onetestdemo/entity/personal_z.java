package cn.hyjdym.onetestdemo.entity;

public class personal_z {//完整的用户信息表
private String name;
private String favor;
private String profession;
private String qinaming;

private String fun;//关注数量
private String star;//被关注 //粉丝关系表

    private String answercount;//回答数量 //reply表

    private String praisecount;//被赞的数量//interinfo表
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFavor() {
        return favor;
    }

    public void setFavor(String favor) {
        this.favor = favor;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getQinaming() {
        return qinaming;
    }

    public void setQinaming(String qinaming) {
        this.qinaming = qinaming;
    }

    public String getPraisecount() {
        return praisecount;
    }

    public void setPraisecount(String praisecount) {
        this.praisecount = praisecount;
    }

    public String getFun() {
        return fun;
    }

    public void setFun(String fun) {
        this.fun = fun;
    }

    public String getStar() {
        return star;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public String getAnswercount() {
        return answercount;
    }

    public void setAnswercount(String answercount) {
        this.answercount = answercount;
    }
}
