package cn.hyjdym.onetestdemo.repository;

import cn.hyjdym.onetestdemo.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,String> {
    User findByUsername(String username);

}
