/*
 Navicat Premium Data Transfer

 Source Server         : 123456
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : localhost:3306
 Source Schema         : mysqlkcsj

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 16/01/2021 21:52:23
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for interinfo
-- ----------------------------
DROP TABLE IF EXISTS `interinfo`;
CREATE TABLE `interinfo`  (
  `praise_user` varchar(255) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL COMMENT '点赞的用户',
  `problem_id` varchar(11) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL COMMENT '问题id',
  `praised_user` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT '被赞的用户',
  PRIMARY KEY (`praise_user`, `praised_user`, `problem_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of interinfo
-- ----------------------------
INSERT INTO `interinfo` VALUES ('user3', '6', 'user2');

-- ----------------------------
-- Table structure for personalinfo
-- ----------------------------
DROP TABLE IF EXISTS `personalinfo`;
CREATE TABLE `personalinfo`  (
  `qianming` varchar(255) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL COMMENT '个性签名',
  `profession` varchar(255) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL,
  `favor` varchar(255) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL COMMENT '爱好',
  `birthday` date NULL DEFAULT NULL,
  `user` varchar(50) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL COMMENT '用户名',
  `cantalk` varchar(3) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL COMMENT '用户是否禁言1 0',
  `name` varchar(255) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL,
  PRIMARY KEY (`user`) USING BTREE,
  INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of personalinfo
-- ----------------------------
INSERT INTO `personalinfo` VALUES ('我是个没有感情的工具人', '键盘侠', '逛知乎', NULL, 'user1', '1', '初始用户1');
INSERT INTO `personalinfo` VALUES ('很忙，你懂？', '大学生', '唱跳rap', '2021-01-08', 'user2', '1', '白天不懂黑夜的痛');
INSERT INTO `personalinfo` VALUES ('sdsd', 'sdasdaaaaa', 'dsad', NULL, 'user3', '1', 'sadsad');

-- ----------------------------
-- Table structure for replyinfo
-- ----------------------------
DROP TABLE IF EXISTS `replyinfo`;
CREATE TABLE `replyinfo`  (
  `reply_text` varchar(255) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL COMMENT '回答内容',
  `reply_user` varchar(255) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL COMMENT '回答问题的人',
  `praisecount` int(10) NOT NULL DEFAULT 0,
  `createtime` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '回答时间',
  `promblem_id` varchar(255) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL COMMENT '问题编号',
  PRIMARY KEY (`promblem_id`, `reply_user`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of replyinfo
-- ----------------------------
INSERT INTO `replyinfo` VALUES ('伤筋断骨一百天，小心点', 'user2', 0, '2021-01-08 19:38:09', '6');
INSERT INTO `replyinfo` VALUES ('冰水敷一下', 'user3', 0, '2021-01-10 11:40:19', '6');

-- ----------------------------
-- Table structure for sandf
-- ----------------------------
DROP TABLE IF EXISTS `sandf`;
CREATE TABLE `sandf`  (
  `star_user` varchar(255) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL COMMENT '明星',
  `fun_user` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT '粉丝',
  PRIMARY KEY (`fun_user`, `star_user`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sandf
-- ----------------------------
INSERT INTO `sandf` VALUES ('user2', 'user1');
INSERT INTO `sandf` VALUES ('user3', 'user2');
INSERT INTO `sandf` VALUES ('user2', 'user3');

-- ----------------------------
-- Table structure for solve
-- ----------------------------
DROP TABLE IF EXISTS `solve`;
CREATE TABLE `solve`  (
  `issolve` int(3) NOT NULL,
  `solveway` varchar(255) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL,
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `problem_remark` varchar(255) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL,
  `redu` int(20) NOT NULL DEFAULT 0,
  `askman` varchar(255) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL,
  `name` varchar(255) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL,
  `id` int(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = gbk COLLATE = gbk_chinese_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of solve
-- ----------------------------
INSERT INTO `solve` VALUES (0, NULL, '2021-01-08 18:46:13', '红肿的很厉害，怎么办，求教!', 10, '张小春', '跑步伤到了腿', 6);
INSERT INTO `solve` VALUES (0, NULL, '2021-01-10 21:28:24', '馋不哭小孩，太失败了', 0, '白天不懂黑夜的痛', '西红柿炒蛋怎么做才好吃？', 8);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `userpassword` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `userrights` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `rolenumber` int(11) NOT NULL,
  `rolename` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `roledescription` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `usercheck` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `useremail` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`userid`) USING BTREE,
  INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (16, 'user2', 'b91e37f96f8d1ef992537f0f23437df1', '1', 1, '1', '普通注册->邮箱验证成功', '', '1148370650@qq.com');
INSERT INTO `user` VALUES (17, 'user3', '902581d6c19fa91d077aaa0ce13bbeca', '1', 1, '1', '普通注册->邮箱验证成功', '', '1148370650@qq.com');
INSERT INTO `user` VALUES (18, 'admin1', 'a7644ae44272edadf0b8d2260bdb5e12', '2', 1, '1', '普通注册->邮箱验证成功', '', '1148370650@qq.com');
INSERT INTO `user` VALUES (19, 'user1', '9ab2e08aee1059b05eff0399adcfc1f2', '1', 1, '1', '普通注册->邮箱验证成功', '', '1148370650@qq.com');
INSERT INTO `user` VALUES (20, 'user4', 'b5fd4eee4eb98b2f33fd4472a2071d65', '1', 1, '1', '普通注册->邮箱验证成功', '', '1148370650@qq.com');

SET FOREIGN_KEY_CHECKS = 1;
